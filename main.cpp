#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include"Process.hpp"
#include"MFQS.hpp"
#include"RTS.hpp"
#include"WHS.hpp"
#include <boost/algorithm/string.hpp>

using namespace std;

//#define DEBUG

struct arrivalKey
{
    inline bool operator() (const Process* p1, const Process* p2)
    {
        return (p1->getArrivalTime() < p2->getArrivalTime());
    }
};

//Helper methods for calculating statistics
double calcAvgWaitTime(int totalJobs, vector<Process*> processes);
double calcAvgTurnTime(int totalJobs, vector<Process*> processes);

int main(int argc, char** argv) {
  string filename = "";
  string algorithm = "";

  //Check to make sure only one filename was entered
  if (argc == 2) {
    filename = string(argv[1]);
  } else {
    cout << "Usage:" << endl << "./main ListOfProcesses.txt" << endl;
    exit(1);
  }

  //Determine what algorithm will be used
  cout << "Which algorithm would you like to use? (rts/whs/mfqs): ";
  cin >> algorithm;
  //Probably should check to make sure the correct algorithm is entered here

  int numQueues = 0;
  int quantum = 0;

  while(algorithm != "rts" && algorithm != "mfqs" && algorithm != "whs"){
    cout << "Please pick one of the listed options and try again:" << endl;
    cin >> algorithm;
  }

  /*
  We know at this point that the user entered one of the valid options.
  It doesn't matter which right now, we'll check later.  Just read in the
  file now.
  */

  //Read in the file
  ifstream processList;
  processList.open(filename);
  //used to store one line at a time
  string line;
  //temp vector used to store the values
  vector<string> values;
  vector<Process*> processes;
  int pid, burst, arrival, priority, deadline, io;
  //eliminate the headers
  getline(processList, line);
  //Go through every process
  bool skip;
  while(getline(processList, line)) {
    //set skip to false
    skip = false;
    //Seperate line vaules by tabs.
    boost::split(values, line, boost::is_any_of("\t"));

    //assign each empty int a value
    pid = atoi(values.at(0).c_str());
    burst = atoi(values.at(1).c_str());
    arrival = atoi(values.at(2).c_str());
    priority = atoi(values.at(3).c_str());
    deadline = atoi(values.at(4).c_str());
    io = atoi(values.at(5).c_str());

    //ignore these lines for all schedulers
    if(arrival < 0) {
      skip = true;
    }
    if(burst <= 0) {
      skip = true;
    }

    //Ignore this just for RTS
    if((algorithm == "rts") && ((deadline < 0) || (burst > deadline) || (deadline < arrival))) {
      skip = true;
    }

    if(((algorithm == "mfqs") || (algorithm == "whs")) && (io < 0)) {
      skip = true;
    }

    //Create a process and add it to the vector
    if(!skip){
      Process *currentProcess = new Process(pid,burst,arrival,priority,deadline,io);
      processes.push_back(currentProcess);
    }
  }
  processList.close();

  //Now we have the vector of processes, we can decide which algorithm to use
  //and construct the class by passing it the vector of processes.

  double avgWaitTime = 0;
  double avgTurnTime = 0;
  //We're going to sort it based on arrival time
  std::sort (processes.begin(), processes.end(), arrivalKey());

  if (algorithm == "rts") {
    cout << "You picked " << algorithm << endl;

    RTS rts = RTS(processes);
    rts.Run();
    int totalJobs = rts.getJobsAttempted();

    avgWaitTime = calcAvgWaitTime(totalJobs, processes);
    avgTurnTime = calcAvgTurnTime(totalJobs, processes);

    cout << "The Average Wait time was: " << avgWaitTime << endl;

    cout << "The Average Turnaround time: " << avgTurnTime << endl;

    cout << "The Number of Jobs Scheduled: " << totalJobs << endl;

    //do something

  } else if (algorithm == "mfqs") {
    cout << "You picked " << algorithm << endl <<
    "Please enter the number of queues you would like to use:" << endl
    << "(This number must be <= 5 and > 0)" << endl;

    int numQueues = 0;
    std::string temp = "";
    cin >> temp;
    numQueues = atoi(temp.c_str());

    while(numQueues > 5 || numQueues <= 0){
      cout << "Number of Queues cannot be greater than 5 but must be greater than 0. " <<
      "Please re-enter a valid number:" << endl;
      cin >> temp;
      numQueues = atoi(temp.c_str());
    }

    int age = 0;
    cout << "How long should a process wait in a queue?" << endl;
    temp = "";
    cin >> temp;
    age = atoi(temp.c_str());

    MFQS mfqs = MFQS(processes, numQueues, age);
    mfqs.Run();

    int totalJobs = mfqs.getJobsScheduled();

    avgWaitTime = calcAvgWaitTime(totalJobs, processes);
    avgTurnTime = calcAvgTurnTime(totalJobs, processes);

    cout << "The Average Wait time was: " << avgWaitTime << endl;
    cout << "The Average Turnaround time: " << avgTurnTime << endl;
    cout << "The Number of Jobs Scheduled: " << totalJobs << endl;

  } else if (algorithm == "whs") {
    cout << "You picked " << algorithm << endl;
    //do something
    cout << "Unfortunately this scheduler could not be completed as dialed."<<endl;
  }

  return 0;
}

double calcAvgWaitTime(int totalJobs, vector<Process*> processes){
  double combinedWait= 0;
  double average = 0;
  //loop through process vector and gather all of the wait times
  for(int i=0; i<processes.size(); i++){
    combinedWait += processes.at(i)->getTimeWaiting();
  }
#ifdef DEBUG
  cout << "Wait: " << combinedWait << " totalJobs: " << totalJobs << endl;
#endif
  //compute the average
  average = combinedWait / totalJobs;
  return average;
}

double calcAvgTurnTime(int totalJobs, vector<Process*> processes){
  double totalTurnTime = 0;
  int tempTurnTime = 0;
  int count = 0;
  double average = 0;

  //Grab the turnaround time for each process
  for(int i=0; i<processes.size(); i++){
    if(processes.at(i)->getTimeCompleted() != 0){
      tempTurnTime = processes.at(i)->getTimeCompleted() - processes.at(i)->getArrivalTime();
      //cout << "Process: PID: " << processes.at(i)->getPID() << " Turnaround time: " << tempTurnTime << endl;
      totalTurnTime += tempTurnTime;
      count++;
    }
  }
  //cout << totalTurnTime << endl;
  //cout << (count) << endl;
  average = totalTurnTime / count;

  return average;
}
