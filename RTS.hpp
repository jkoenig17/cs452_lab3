#ifndef RTS_HPP
#define RTS_HPP

#include<deque>
#include<queue>
#include<deque>
#include<list>
#include"Process.hpp"
#include<vector>

class RTS {

private:
  std::vector<Process*> processes;
  std::vector<Process*> finishedProcesses;
  int jobsAttempted = 0;

public:
  RTS(std::vector<Process*> processes);
  int getJobsAttempted() const { return jobsAttempted; }
  void Run();
};

#endif
