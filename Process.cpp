#include "Process.hpp"

//Process Constructors

Process::Process(){}

Process::Process(int pid, int burst, int arrivalTime, int priority, int deadline, int io) {
  this->pid = pid;
  this->burst = burst;
  this->arrivalTime = arrivalTime;
  this->priority = priority;
  this->deadline = deadline;
  this->io = io;
  this->timeRemaining = burst;
}

void Process::setTimeRemaining(int time){
  this->timeRemaining = time;
}

void Process::setPriority(int priority){
  this->priority = priority;
}

void Process::setTimeWaiting(int time){
  this->timeWaiting = time;
}

void Process::setTimeCompleted(int time) {
  this->timeCompleted = time;
}

void Process::setCurrentQueue(int queue){
  this->currentQueue = queue;
}

void Process::setIO(int io){
  this->io = io;
}

void Process::setStart(int start){
  this->start = start;
}
