#ifndef WHS_HPP
#define WHS_HPP

#include<deque>
#include<queue>
#include<list>
#include"Process.hpp"
#include<vector>

class WHS {

private:
  std::vector<Process*> processes;

public:
  WHS(std::vector<Process*> processes);
};

#endif
