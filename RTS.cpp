#include "RTS.hpp"
#include <iostream>
#include <string>
#include<algorithm>
#include "Process.hpp"

#define DEBUG

using namespace std;

struct deadLineKey
{
    inline bool operator() (const Process* p1, const Process* p2)
    {
        return (p1->getDeadline() < p2->getDeadline());
    }
};

RTS::RTS(std::vector<Process*> processes){
  this->processes = processes;

}
void RTS::Run() {
  int clock = 0;
  //This queue is for holding process that have entered the system
  deque<Process*> rtsQueue;
  //variable for determining whether it is hard or soft
  string softOrHard;
  short isHard = 0;

  //Ask user whether they would like a hard or soft system.
  cout << "Please enter either \"soft\" or \"hard\" to indicate what kind of system you would like: " << endl;
  cin >> softOrHard;
  if(softOrHard == "hard"){
    isHard = 1;
  }

  //Start at the first process because why waste clock ticks?
  clock = processes.at(0)->getArrivalTime();
  Process *runningProcess = NULL;
  int processIterator = 0;
  //Take everything with the earliest initial arrival time and push it onto the queue.
  while(processes.at(processIterator)->getArrivalTime() == clock) {
    rtsQueue.push_back(processes.at(processIterator));
    processIterator++;
  }
  //sort the queue by deadLine
  sort(rtsQueue.begin(), rtsQueue.end(), deadLineKey());

  //Then, assign the first thing in the queue to runningProcess, and pop it off the queue.
  runningProcess = rtsQueue.front();
  jobsAttempted++;
  rtsQueue.pop_front();

  while (!rtsQueue.empty() || processIterator < processes.size() || runningProcess != NULL) {
#ifdef DEBUG
  //Print out the current clock
  cout << "Clock: " << clock << " Process: PID: " << runningProcess->getPID() << ", Time Remaining: " << runningProcess->getTimeRemaining() << ", deadLine: " << runningProcess->getDeadline() << endl;
  cout << "rtsQueue: ";
  for(int i = 0; i < rtsQueue.size(); i++) {
    cout << rtsQueue.at(i)->getPID() << ", ";
  }
  cout << endl;
#endif

  //check to see if a process cant finish
  while(runningProcess != NULL && runningProcess->getTimeRemaining() + clock > runningProcess->getDeadline()){
    //Drop it as NOT completed and get nExt process from queue
#ifdef DEBUG
  cout << "Process: PID: " << runningProcess->getPID() << " stopped. " << "Clock: " << clock << ", Time Remaining: " << runningProcess->getTimeRemaining() << ", deadLine: " << runningProcess->getDeadline() << endl;
#endif

    if(runningProcess->getTimeRemaining() != runningProcess->getBurst()) {
      jobsAttempted++;
    }
    if(rtsQueue.size() > 0){
      runningProcess = rtsQueue.front();
      rtsQueue.pop_front();
    } else {
      runningProcess = NULL;
    }
    //Check if its a hard environment.  If it is, stop everything because hard.
    if(isHard == 1){
      cout << "Since we are in a hard environment, we are stopping the system right here." << endl;
      exit(1);
    }
  }
    //Perform the clock tick functions
    //decrement the time remaning by 1
    if(runningProcess != NULL){
      runningProcess->setTimeRemaining(runningProcess->getTimeRemaining() -1);
    }
    //go through everything the the rtsQueue and increase their wait time by 1
    for(int i = 0; i < rtsQueue.size(); i++){
      Process* temp = rtsQueue.at(i);
      temp->setTimeWaiting(temp->getTimeWaiting() + 1);
    }

    clock++;
    if(runningProcess != NULL && runningProcess->getTimeRemaining() == 0){
      //The process is done running, add it to finished processes and
      //drop it and grab new process from queue.
      runningProcess->setTimeCompleted(clock);
      jobsAttempted++;
      if(rtsQueue.size() > 0) {
        runningProcess = rtsQueue.front();
        rtsQueue.pop_front();
      }else if(processIterator < processes.size()){
        clock = processes.at(processIterator)->getArrivalTime();
        runningProcess = NULL;
      } else {
        runningProcess = NULL;
      }
    } else if(runningProcess == NULL && processIterator < processes.size()){

      clock = processes.at(processIterator)->getArrivalTime();
    }

    //check to see if a process has arrived
    if(processIterator < processes.size() && clock == processes.at(processIterator)->getArrivalTime()){
      //Put current process back on the queue
      if(runningProcess != NULL){
        rtsQueue.push_back(runningProcess);
      }

      //Push the arrived proceses onto the queue
      while(processIterator < processes.size() && processes.at(processIterator)->getArrivalTime() == clock) {
        rtsQueue.push_back(processes.at(processIterator));
        processIterator++;
      }

      //sort the list by deadline and then grab the one with the lowest deadline
      sort(rtsQueue.begin(), rtsQueue.end(), deadLineKey());
      runningProcess = rtsQueue.front();
      rtsQueue.pop_front();


    }

  }
    //Re-sort the vector based on deadlines, becuase we better get shit done before then
    std::sort (processes.begin(), processes.end(), deadLineKey());
}
