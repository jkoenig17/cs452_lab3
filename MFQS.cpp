#include"MFQS.hpp"
#include<string>
#include<iostream>
#include<unistd.h>

#define DEBUG

MFQS::MFQS(std::vector<Process*> processes, int numQueues, int age){
  this->age = age;
  this->numQueues = numQueues;
  this->processes = processes;
  //set up the queues
  for(int i = 0; i < numQueues; i++){
    std::deque<Process*> q;
    queues.push_back(q);
  }
}

void MFQS::Run(){
  int clock = this->processes.at(0)->getArrivalTime();
  this->processIterator = 0;

  //Add as many processes as have the lowest arrival time to Q0 and set their Queue value
  for(int i = 0; clock == processes.at(i)->getArrivalTime(); i++){
    queues.at(0).push_back(processes.at(i));
    queues.at(0).back()->setCurrentQueue(0);
    this->processIterator++;
  }
  #ifdef DEBUG
  printQueues();
  #endif

  //Get the process set up the first time before the main loop
  Process *runningProcess = queues.at(0).front();
  jobsScheduled++;
  int currentQuantum = timeQuantum;
  bool pop = true;

  //Main loop
  while(!queuesAreEmpty() || this->processIterator < processes.size()){
    pop = true;
    #ifdef DEBUG
    printQueues();
    #endif

    //Before anything check if the process does I/O and move it to the wait queue
    if(runningProcess->getIO() > 0){
      ioQueue.push_back(runningProcess);

      //Process not in last queue
    }else if(runningProcess->getCurrentQueue() < (this->numQueues) - 1){
      //Set the quantum based on what queue the process is in
      currentQuantum = setCurrentQuantum(runningProcess);
      bool processFinished = false;

      while(currentQuantum > 0 ){
        if(runningProcess->getTimeRemaining() == 0){
          //The process finished running, record the time
          runningProcess->setTimeCompleted(clock);
          processFinished = true;
          break;
        }
        currentQuantum--;
        int temp = runningProcess->getTimeRemaining();
        runningProcess->setTimeRemaining(temp - 1);

        incrementWaiting();
        decrementIO();
        checkVector(clock);
        clock++;
      #ifdef DEBUG
        std::cout << "Clock: " << clock <<std::endl;
      #endif
      }if(!processFinished){
        //Process didn't finish, demote accordingly
        int q = runningProcess->getCurrentQueue();
        if(q < numQueues){
            this->queues.at(runningProcess->getCurrentQueue()).pop_front();
            runningProcess->setCurrentQueue(q + 1);
            this->queues.at(q + 1).push_back(runningProcess);
            pop = false;
        }
      }

      //The process IS in the last queue, run til you drop
    }else if(runningProcess->getCurrentQueue() == this->numQueues){
      while(runningProcess->getTimeRemaining() > 0){
        int temp = runningProcess->getTimeRemaining();
        runningProcess->setTimeRemaining(temp - 1);
        incrementWaiting();
        decrementIO();
        checkVector(clock);
        clock++;
        #ifdef DEBUG
        std::cout << "Clock: " << clock <<std::endl;
        #endif
      }
      //at this point, the process should be finished and time remaining should = 0
      runningProcess->setTimeCompleted(clock);
      #ifdef DEBUG
      std::cout << "Start: " << runningProcess->getStart() << std::endl;
      std::cout << "End: " << runningProcess->getTimeCompleted() << std::endl;
      #endif
    }
      //after all
      incrementWaiting();
      decrementIO();
      checkVector(clock);
      clock++;
      #ifdef DEBUG
      std::cout << "Clock: " << clock <<std::endl;
      #endif

      if(pop){
      this->queues.at(runningProcess->getCurrentQueue()).pop_front();
      }

      while(queuesAreEmpty() && (!(this->ioQueue.empty()) || !(this->processIterator >= this->processes.size()))){
        incrementWaiting();
        decrementIO();
        #ifdef DEBUG
        printQueues();
        #endif
        checkVector(clock);
        clock++;
        #ifdef DEBUG
        std::cout << "Clock: " << clock <<std::endl;
        #endif
      }
      if(!queuesAreEmpty()){
        runningProcess = getNextProcess();
        runningProcess->setStart(clock);
        jobsScheduled++;
      }
      //end big while loop
    }
    //end method
  }



//HELPER METHODS


//Checks if all the queues in queues vector are empty
bool MFQS::queuesAreEmpty(){
  for(int i = 0; i < this->queues.size(); i++){
    if(!(this->queues.at(i).empty())){
      return false;
    }
  }
  return true;
}

//Prints out everything currently in the priority queues
void MFQS::printQueues(){
  for(int i = 0; i < this->queues.size(); i++){
    std::cout << "Q" << i << " " << this->queues.at(i).size() << std::endl;
    for(int j = 0; j < this->queues.at(i).size(); j++){
      std::cout << "PID: " <<  this->queues.at(i).at(j)->getPID() << "  ";
    }
    std::cout << std::endl;
    std::cout << std::endl;
  }
}

//Calculates the time quantum for a process based on what queue the process is in
int MFQS::setCurrentQuantum(Process *process){
  int currentQuantum = this->timeQuantum;
  for(int i = 0; i < process->getCurrentQueue(); i++){
    currentQuantum = currentQuantum / 2;
  }
  return currentQuantum;
}

//Returns the first process of the first populated queue
Process* MFQS::getNextProcess(){
  Process *process = NULL;
    for(int i = 0; i < this->numQueues; i++){
      if(!(this->queues.at(i)).empty()){
        process = this->queues.at(i).front();
        break;
      }
    }
  return process;
}

//Do everything that NEEDS to be done at the end of every clock tick
void MFQS::incrementWaiting(){
  for(int i = 0; i < this->numQueues; i++){
    for(int j = 0; j < this->queues.at(i).size(); j++){
      int waitTime = this->queues.at(i).at(j)->getTimeWaiting();
      this->queues.at(i).at(j)->setTimeWaiting(waitTime + 1);
    }
  }
}

void MFQS::decrementIO(){
  int i = 0;
  while(i < this->ioQueue.size()){
    int ioVal = this->ioQueue.at(i)->getIO();
    this->ioQueue.at(i)->setIO(ioVal - 1);
    if(this->ioQueue.at(i)->getIO() == 0){
      this->queues.at(0).push_back(ioQueue.at(i));
      this->queues.at(0).back()->setCurrentQueue(0);
      ioQueue.erase(ioQueue.begin() + i);
    }else{
      i++;
    }
  }
}

void MFQS::checkVector(int clock){
  if(this->processIterator < this->processes.size()){
    while(this->processIterator < this->processes.size() && this->processes.at(this->processIterator)->getArrivalTime() == clock){
      this->queues.at(0).push_back(this->processes.at(this->processIterator));
      this->queues.at(0).back()->setCurrentQueue(0);
      this->processIterator++;
    }
  }
  #ifdef DEBUG
    printQueues();
    #endif
}
