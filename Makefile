
scheduler: main.cpp Process.cpp RTS.cpp
	g++ main.cpp Process.cpp RTS.cpp MFQS.cpp WHS.cpp -std=c++11 -o main
clean:
	rm -f *.o main.cpp~ Makefile~ main README.txt~ Process.cpp~ RTS.cpp~ WHS.cpp~ MFQS.cpp~
debug:
	g++ main.cpp Process.cpp RTS.cpp MFQS.cpp WHS.cpp -std=c++11 -g -o main
