To compile the program:

run "make"

This will create "main"

To Clean:

Simply run "make clean"

To Run:
Run ./main, giving it a file containing a list of processes. ex: "./main 10kProcesses.txt"
