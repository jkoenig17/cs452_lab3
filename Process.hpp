#ifndef PROCESS_HPP
#define PROCESS_HPP

class Process {

  private:
    int pid;
    int burst;
    int timeRemaining;
    int timeWaiting = 0;
    int timeCompleted = 0;
    int arrivalTime;
    int priority;
    int deadline;
    int io;
    int currentQueue;
    int start;

  public:
    Process();
    Process(int pid, int burst, int arrivalTime, int priority, int deadline, int io);
    int getPID() const { return pid; }
    int getBurst() const { return burst; }
    int getArrivalTime() const { return arrivalTime; }
    int getTimeRemaining() const { return timeRemaining; }
    int getTimeWaiting() const { return timeWaiting; }
    int getTimeCompleted() const { return timeCompleted; }
    int getPriority() const { return priority; }
    int getDeadline() const { return deadline; }
    int getIO() const { return io; }
    void setTimeRemaining(int time);
    void setPriority(int priority);
    void setTimeWaiting(int time);
    void setTimeCompleted(int time);
    void setIO(int io);
    int getCurrentQueue() const {return currentQueue;}
    int getStart() const {return start;}
    void setStart(int start);
    void setCurrentQueue(int queue);

};

#endif
