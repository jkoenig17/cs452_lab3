#ifndef MFQS_HPP
#define MFQS_HPP

#include<deque>
#include<queue>
#include<list>
#include"Process.hpp"
#include <vector>

class MFQS {

private:
  int age;
  int numQueues;
  std::vector<Process*> processes;
  std::vector<std::deque<Process*>> queues;
  std::vector<Process*> ioQueue;
  int jobsScheduled = 0;
  int timeQuantum = 16;
  int processIterator;
  bool queuesAreEmpty();
  void printQueues();
  int getQueueNumber();
  int setCurrentQuantum(Process *process);
  void incrementWaiting();
  void decrementIO();
  void checkVector(int clock);
  Process* getNextProcess();

public:
  MFQS(std::vector<Process*> processes, int numQueues, int age);
  int getJobsScheduled() const { return jobsScheduled; }
  void Run();
};

#endif
